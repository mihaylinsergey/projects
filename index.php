        <!--Шапка сайта-->
        <?php get_header(); ?>
        <!--Конец шапки сайта-->

        <!--Блок сменного контента(Главная страница)-->
        <div class="row content">
            
            <div class="col-12 banner">
                <div class="left_banner">
                    <span>
                        Совершенно<br>
                        иные<br>
                        технологии<br>
                        распыления
                    </span>
                </div>
                <div class="center_banner">
                    <div class="paint"><span>Paint</span></div>
                    <div class="decoration_line"></div>
                    <div class="drop"><span>"Drop"</span></div>
                </div>
                <div class="right_banner">
                    <span>
                        Совершенно<br>
                        иной<br>
                        наносостав<br>
                        краски   
                    </span>
                </div>
            </div>
            <!--Карусель-->
            <div class="col-12 karusel">
                <div class="karusel_wrapper">

                    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                          <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                          <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                          <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                        </ol>
                        <div class="carousel-inner">
                          <div class="carousel-item active">
                            <img src="<?php echo get_stylesheet_directory_uri();?>/img/0.png" class="d-block w-100" alt="...">
                          </div>
                          <div class="carousel-item">
                            <img src="<?php echo get_stylesheet_directory_uri();?>/img/1.png" class="d-block w-100" alt="...">
                          </div>
                          <div class="carousel-item">
                            <img src="<?php echo get_stylesheet_directory_uri();?>/img/2.png" class="d-block w-100" alt="...">
                          </div>
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                          <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                          <span class="carousel-control-next-icon" aria-hidden="true"></span>
                          <span class="sr-only">Next</span>
                        </a>
                      </div>

                </div>
            </div>
            <!--Конец карусели-->

            <div class="col-12 information">
                <div class="col-12 buy">
                    <a href="#"><p>Заказать краску</p></a></div>
                <div class="col-12 text_buy">
                    <p>
                        Текст мелким шрифтом, который толком никто и никогда не читает,но ему приходится тут находиться для заполнения пространства,ибо без него это место<br>
                        кажется скучным и пустым.Типа понаписали чего то важного,но по факту бред бредом.Чушь отборная. И вот я не могу придумать подобного текста для третьей<br>
                        строчки.Тоже самое я испытываю при написании четвертой строки.При этом важно подметить,что вам придется дочитать это,если вы уже начали,как<br>
                        минимум по причине того,что начатое нельзя бросать на половине пути.Пожалуй хватит заполнения. Желаем творческих успехов или как то так!
                    </p>
                </div>
            </div>

        </div>
        <!--Конец блока сменного контента-->

        <!--Подвал сайта-->
        <?php get_footer(); ?>
        <!--Конец подвала сайта-->