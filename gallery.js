<script>
    window.onload = function () {
        let img_click = document.querySelectorAll('.img_click');
        let gallery_modal_window = document.querySelector(".gallery_modal_window");

        for (let elem of img_click) {
            elem.onclick = function(){
                let gallery_modal_window_img = document.querySelector('.gallery_modal_window_img');
                gallery_modal_window_img.src = elem.currentSrc;
                gallery_modal_window.style.display = "block";
            }
        }  

        let close_gallery_modal_window = document.querySelector(".close_gallery_modal_window");
        close_gallery_modal_window.onclick = function(){
            gallery_modal_window.style.display = "none";
        }   
    }
</script>